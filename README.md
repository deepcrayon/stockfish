# Stockfish for Cartesi RISC-V Machine Emulator
This branch contains patchlets to compile a RISC-V 64-bit build of
stockfish to run under the Cartesi machine emulator.

# Setup Cartesi SDK
Build using Cartesi GCC. I just copied them from the docker build
and put them in my path (outside of the docker). It's probably
easiest to build in the SDK docker.

```
git clone --recursive https://github.com/cartesi/machine-emulator-sdk
```

You should be able
to run this command OK before proceeding:

```
riscv64-cartesi-linux-gnu-gcc --version
```

# Build
Note, the Makefile kludge disables things like "tests".

Run this to build stockfish binary for riscv64 using Cartesi GCC toolchain:

```
git clone https://spacecruft.org/deepcrayon/stockfish
cd stockfish/src
make -j build ARCH=riscv64 COMP=riscv64-cartesi-linux-gnu-gcc COMPCXX=riscv64-cartesi-linux-gnu-g++
```

# Build Disk Images for Cartesi Emulator

```
# Go back to top level directory in this git repo:
cd ..

# Copy newly built stockfish for cartesi disk image:
cp -p src/stockfish cartesi/img/stockfish

# Make 50 megabyte ext2 disk image with stockfish and shell script
genext2fs -b 51200 -d cartesi/img cartesi/stockfish.ext2
```

# Run Stockfish in Emulator
To run the disk images, using upstream rootfs:

```
cartesi-machine								\
	--ram-length=256Mi						\
	--flash-drive=label:stockfish,filename:cartesi/stockfish.ext2	\
	--flash-drive=label:root,filename:/opt/cartesi/share/images/rootfs.ext2	\
	-- /mnt/stockfish/stockfish.sh
```

# Emulator Output
This takes ~3 minutes to run:

```
         .
        / \
      /    \
\---/---\  /----\
 \       X       \
  \----/  \---/---\
       \    / CARTESI
        \ /   MACHINE
         '


Stockfish running under Cartesi virtual machine

Stockfish 161121 by the Stockfish developers (see AUTHORS file)

option name Debug Log File type string default 
option name Threads type spin default 1 min 1 max 512
option name Hash type spin default 16 min 1 max 33554432
option name Clear Hash type button
option name Ponder type check default false
option name MultiPV type spin default 1 min 1 max 500
option name Skill Level type spin default 20 min 0 max 20
option name Move Overhead type spin default 10 min 0 max 5000
option name Slow Mover type spin default 100 min 10 max 1000
option name nodestime type spin default 0 min 0 max 10000
option name UCI_Chess960 type check default false
option name UCI_AnalyseMode type check default false
option name UCI_LimitStrength type check default false
option name UCI_Elo type spin default 1350 min 1350 max 2850
option name UCI_ShowWDL type check default false
option name SyzygyPath type string default <empty>
option name SyzygyProbeDepth type spin default 1 min 1 max 100
option name Syzygy50MoveRule type check default true
option name SyzygyProbeLimit type spin default 7 min 0 max 7
option name Use NNUE type check default true
option name EvalFile type string default nn-13406b1dcbe0.nnue
uciok

Halted
Cycles: 3142947347
```

# Store Machine State
Instead of fully booting, a RAM image can be loaded and the
CPU set to a cycle, such as 3142947347. The below command boots
the image and runs until the 3142947347th cycle (when above terminates,
could be earlier), then halts the machine, saving the state of RAM to
a directory.


```
cartesi-machine								\
	--max-mcycle=3142947347						\
	--store=cartesi/stockfish-ram					\
	--ram-length=256Mi						\
	--flash-drive=label:stockfish,filename:cartesi/stockfish.ext2	\
	--flash-drive=label:root,filename:/opt/cartesi/share/images/rootfs.ext2	\
	-- /mnt/stockfish/stockfish.sh
```

Note, it won't overwrite the the directory, so you may need to delete
the above directory if you re-run the above command:

```
# Only run if you re-run above:
rm -rf cartesi/stockfish-ram
```

# Load Machine State
This loads the above machine state and starts it at the begining,
running until the end. Basically, finishes at the end.

```
cartesi-machine								\
	--load=cartesi/stockfish-ram
```

# Store/Load Machine State Mid-Run
This stores the machine state halfway through normal completion.

```
cartesi-machine								\
	--max-mcycle=1571473674						\
	--store=cartesi/stockfish-ram-mid				\
	--ram-length=256Mi						\
	--flash-drive=label:stockfish,filename:cartesi/stockfish.ext2	\
	--flash-drive=label:root,filename:/opt/cartesi/share/images/rootfs.ext2	\
	-- /mnt/stockfish/stockfish.sh
```


This loads the above, and starts it in the middle cycle.
Load and run it from halfway:
```
cartesi-machine								\
	--load=cartesi/stockfish-ram-mid
```

# Store/Load Machine State 95% Run
This stores the machine state 95% through normal completion.
This is like a "fast boot" to the application.

```
cartesi-machine								\
	--max-mcycle=2985799980						\
	--store=cartesi/stockfish-ram-95				\
	--ram-length=256Mi						\
	--flash-drive=label:stockfish,filename:cartesi/stockfish.ext2	\
	--flash-drive=label:root,filename:/opt/cartesi/share/images/rootfs.ext2	\
	-- /mnt/stockfish/stockfish.sh

```


This loads the above, and should finish relatively fast.
Doing a full run takes around 3 minutes to get the full test
output from stockfish. Running below takes 18 seconds to get the the
identical output. It could be made much faster by finding the exact
cycle where the output begins.

```
cartesi-machine								\
	--load=cartesi/stockfish-ram-95
```

# TODO
* Fix tests.

# Upstream
See:

* README-upstream.md

* https://github.com/official-stockfish/Stockfish

* https://stockfishchess.org/

# Cartesi
See:

* https://cartesi.io/en/docs/intro/

* https://github.com/cartesi/machine-emulator/

# Copying
Same as upstream Stockfish, GPL v3.

# Contact
```
# y@deepcrayon.fish or jebba in Cartesi/Stockfish/lichess discord

```

